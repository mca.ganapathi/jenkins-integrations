#!/bin/bash

echo "Checking the Uptime of the system"
uptime

echo "FSTAB Entries"
cat /etc/fstab

echo "Disk Util status"
df -h

echo "Memory status"
free -m

echo "Dmidecode for hardware info"
sudo dmidecode -t 0

echo "To check the var log"
cat /var/log/secure